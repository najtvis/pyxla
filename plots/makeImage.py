#%% Import libraries
#=======================================================
import matplotlib.pyplot as plt
import numpy as np
import csv
from matplotlib import ticker

imageSize = (256,256)
image = np.zeros(imageSize)
maxValue = 0
fileName = '8keV_1D_3points'
save = False
with open(fileName + '.txt', newline='') as csvfile:
    reader = csv.reader(csvfile, delimiter=' ', quotechar='|')

    #allData = list(reader)
    #image = np.zeros((len(allData),len(allData[0])))
    
    i=0
    for row in reader:
        for j in range(0, len(row)-1): 
            image[j-1][i] = float(row[j]) 
             
            if(image[j-1][i] > maxValue):
                maxValue = image[j-1][i]
        i=i+1
   
for i in range(0, len(image)):
    for j in range(0, len(image[i])): 
        image[i][j] = image[i][j]/maxValue 

image = np.transpose(image)
#maxValue = 39.95188
                
x=np.linspace(0,len(image[0])-1, len(image))

size=13

fig, ax = plt.subplots(figsize=(5.5,3.2), dpi=300)
im = ax.imshow(image, cmap='Greys', vmin=0, vmax=1, extent=[0,14,0,14])	
cbar = fig.colorbar(im, ax=ax)
formatter = ticker.ScalarFormatter(useMathText=True)
formatter.set_scientific(True) 
formatter.set_powerlimits((0,1))
cbar.ax.yaxis.get_offset_text().set_fontsize(0.8*size)
cbar.ax.yaxis.set_major_formatter(formatter) 
cbar.ax.tick_params(labelsize=0.8*size)
cbar.set_label('Normalised intensity (-)', rotation=90, fontsize=size)
plt.xlabel("x (mm)", fontsize=size)
plt.ylabel("y (mm)", fontsize=size)
plt.xticks([0, 2, 4, 6, 8, 10, 12, 14], fontsize=0.8*size)
plt.yticks([0, 2, 4, 6, 8, 10, 12, 14], fontsize=0.8*size)
fig.savefig(fileName + '.eps', format='eps', bbox_inches="tight")
fig.savefig(fileName + '.png', format='png', bbox_inches="tight", dpi=600)
plt.show()


# create sum of rows and columns
x=np.linspace(0, imageSize[0]-1, num=imageSize[0])
arr = np.array(image)
histogramV = [ sum(x) for x in arr ]

arr = np.array(image).T
histogramH = [ sum(x) for x in arr ]


if save == True:
    fig, ax = plt.subplots(figsize=(5.5,3.2), dpi=300)
else:
    fig, ax = plt.subplots(figsize=(5.5,3.2))
    
plt.xlabel("x (px)", fontsize=size)
plt.ylabel("y (px)", fontsize=size)
plt.xticks([0, 50, 100, 150, 200, 250], fontsize=0.8*size)
plt.yticks([0, 50, 100, 150, 200, 250], fontsize=0.8*size)
plt.plot(x, histogramH)
plt.grid(b=True, which='major',  linestyle='-')
plt.grid(b=True, which='minor', linestyle='--')
plt.show()
fig.savefig(fileName + '_sumRows.eps', format='eps', bbox_inches="tight")
fig.savefig(fileName + '_sumRows.png', format='png', bbox_inches="tight", dpi=600)


if save == True:
    fig, ax = plt.subplots(figsize=(5.5,3.2), dpi=300)
else:
    fig, ax = plt.subplots(figsize=(5.5,3.2))
    
plt.plot(x, histogramV)
plt.xlabel("x (px)", fontsize=size)
plt.ylabel("y (px)", fontsize=size)
plt.xticks([0, 50, 100, 150, 200, 250], fontsize=0.8*size)
plt.yticks([0, 50, 100, 150, 200, 250], fontsize=0.8*size)
plt.grid(b=True, which='major', linestyle='-')
plt.grid(b=True, which='minor', linestyle='--')

plt.show()
fig.savefig(fileName + '_sumColumns.eps', format='eps', bbox_inches="tight")
fig.savefig(fileName + '_sumColumns.png', format='png', bbox_inches="tight", dpi=600)
