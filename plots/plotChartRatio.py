#%% 
import csv
from click import style
import numpy as np
import matplotlib.pyplot as plt
from matplotlib import pyplot as rcParams
from matplotlib import cm
import pandas as pd


#from PIL import ImageDraw


def readCSV(fileName):
    with open(fileName, 'r') as f:
        print(fileName)
        # next(f) #skip line
        df = pd.read_csv(f, delimiter=',')
        #saved_column = df.column_name #you can also use df['column_name']
        #reader = [list(map(float,rec)) for rec in csv.reader(f, delimiter=' ')]
        #image = list(reader)
    #print(df)
    return df

def drawCircle(ax, x, y, r, colour):
    circle = plt.Circle((x, y), r, color=colour, fill=False, lw=0.25)
    ax.add_patch(circle)


threshold = 0


energy=0
# Prepare histograms

size=13
#fig, ax = plt.subplots(figsize=(4,2.7), dpi=300)
fig, ax = plt.subplots(figsize=(5.5,3.2))
ax2 = ax.twinx()
#show image
#im = ax.imshow(stp.detector.pix_ar, cmap='Greys', vmin=0, vmax=10)	
name = 'speed_up'
graph = readCSV(name + '.csv')
ax.plot(graph['No Rays'], graph['Time seq'], 'o', color='tab:blue', label='Sequential', markersize=4, markeredgecolor='tab:blue', markerfacecolor='none')

# name = 'R_vs_E_at_0.25deg'
# graph = readCSV(name + '.txt')
ax.plot(graph['No Rays'], graph['Time Par 24 Threads'], 'o', color='tab:blue', label='Parallel', markersize=4)	

ax2.plot(graph['No Rays'], graph['Ratio'], 'x', color='tab:red', label='Ratio', markersize=4)	
# name = 'R_vs_E_at_0.5deg'
# graph = readCSV(name + '.txt')
# plt.plot(graph['Photon Energy (eV)'], graph['Reflectivity'], label='0.50°')	

# name = 'R_vs_E_at_1.0deg'
# graph = readCSV(name + '.txt')
# plt.plot(graph['Photon Energy (eV)'], graph['Reflectivity'], label='1.00°')	

# name = 'R_vs_E'
#ax.legend(loc='uppcer center', bbox_to_anchor=(1, 1.15), ncol=4, prop={'size': 0.8*size})
# plt.legend(loc='upper left', prop={'size': 0.8*size})
lines, labels = ax.get_legend_handles_labels()
lines2, labels2 = ax2.get_legend_handles_labels()
ax2.legend(lines + lines2, labels + labels2, loc='upper left', prop={'size': 0.8*size})
#colorbar
#cbar = fig.colorbar(im, ax=ax)
#cbar.ax.set_yticks([0, 10, 20, 30, 40])
#cbar.ax.tick_params(labelsize=0.8*size)
#cbar.set_label('Intensity (-)', rotation=90, fontsize=size)

ax.set_xlabel("No. samples (-)", fontsize=size)
ax.set_ylabel("Time (s)", fontsize=size, color='tab:blue')
ax2.set_ylabel("Ratio (-)", fontsize=size, color='tab:red')
ax.tick_params(axis='y', colors='tab:blue')
ax2.tick_params(axis='y', colors='tab:red')

ax.set_xticks([0, 1000, 10000, 100000, 1000000, 10000000], fontsize=0.8*size)
ax.set_yticks([0, 200, 400, 600, 800, 1000, 1200, 1400], fontsize=0.8*size, color='tab:blue')
ax2.set_yticks([0, 2, 4, 6, 8, 10, 12, 14], fontsize=0.8*size, color='tab:red')
ax.set_xscale('log')

params = {'legend.fontsize': 'large',
          'figure.figsize': (6,3),
          'axes.labelsize': size,
          'axes.titlesize': size,
          'xtick.labelsize': size*0.75,
          'ytick.labelsize': size*0.75,
          'axes.titlepad': 25}
ax.set_ylim(bottom=0, top=1400)
ax2.set_ylim(bottom=0, top=14)
plt.xlim(left=1000, right=10000000)
ax.grid(b=True, which='major', linestyle='-')
ax.minorticks_on()
ax.grid(b=True, which='minor', linestyle='--', alpha=0.2)
fig.savefig(name + '.eps', format='eps', bbox_inches="tight")
fig.savefig(name + '.png', format='png', bbox_inches="tight", dpi=600)
plt.show()

# %%
