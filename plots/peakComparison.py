#%% Import libraries
#=======================================================
import matplotlib.pyplot as plt
import numpy as np
import src.functions as functions
from matplotlib import ticker
import pandas as pd

image1 = np.zeros((256, 256))
image2 = np.zeros((256, 256))
image3 = np.zeros((256, 256))
image4 = np.zeros((256, 256))

maxValue = 0

image1 = functions.readCSV('outputs/8keV_2D_uniform_no_adj_0.txt') 
image1 = np.array(image1)
maxVal = image1.max()
if(maxVal > maxValue):
    maxValue = maxVal

image2 = functions.readCSV('outputs/8keV_2D_uniform_no_adj_20.txt') 
image2 = np.array(image2)
maxVal = image2.max()
if(maxVal > maxValue):
    maxValue = maxVal

image3 = functions.readCSV('outputs/8keV_2D_uniform_no_adj_29.txt') 
image3 = np.array(image3)
maxVal = image3.max()
if(maxVal > maxValue):
    maxValue = maxVal

image4 = functions.readCSV('outputs/8keV_2D_uniform_no_adj_40.txt') 
image4 = np.array(image4)
maxVal = image4.max()
if(maxVal > maxValue):
    maxValue = maxVal

for i in range(0, len(image1)):
    for j in range(0, len(image1[i])): 
        image1[i][j] = image1[i][j]/maxValue 

for i in range(0, len(image2)):
    for j in range(0, len(image2[i])): 
        image2[i][j] = image2[i][j]/maxValue 

for i in range(0, len(image3)):
    for j in range(0, len(image3[i])): 
        image3[i][j] = image3[i][j]/maxValue 

for i in range(0, len(image4)):
    for j in range(0, len(image4[i])): 
        image4[i][j] = image4[i][j]/maxValue 


x256=np.linspace(0, 0.055*256, 256)
x512=np.linspace(0, 0.055*256, 512)

image1Row = image1[128]
image2Row = image2[128]
image3Row = image3[128]
image4Row = image4[128]

size=13
#fig, ax = plt.subplots(figsize=(4,2.7), dpi=300)
fig, ax = plt.subplots(figsize=(5.5,3.2))

plt.plot(x256, image1Row, label='0 mm')
plt.plot(x256, image2Row, label='20 mm', linestyle = '--')
plt.plot(x256, image3Row, label='29 mm', linestyle = '-.')
plt.plot(x256, image4Row, label='40 mm', linestyle = ':')

# plt.plot(xPyxla2, pyxla2Row, label='Pyxla adj')

ax.legend(loc='upper right', bbox_to_anchor=(1, 1), ncol=1, prop={'size': 0.8*size})

plt.xlabel("x (mm)", fontsize=size)
plt.ylabel("Normalised intensity (-)", fontsize=size)
# plt.xticks([0, 2, 4, 6, 8, 10, 12, 14], fontsize=0.8*size)
plt.xticks([6, 6.5, 7, 7.5, 8], fontsize=0.8*size)
plt.yticks([0, 0.25, 0.5, 0.75, 1], fontsize=0.8*size)

params = {'legend.fontsize': 'large',
          'figure.figsize': (6,3),
          'axes.labelsize': size,
          'axes.titlesize': size,
          'xtick.labelsize': size*0.75,
          'ytick.labelsize': size*0.75,
          'axes.titlepad': 25}
plt.ylim(bottom=0, top=1.05)
plt.xlim(left=6, right=8)
ax.grid(visible=True, which='major', linestyle='-')
plt.minorticks_on()
ax.grid(b=True, which='minor', linestyle='--', alpha=0.2)
fig.savefig('PeakComparison.eps', format='eps', bbox_inches="tight")
fig.savefig('PeakComparison.png', format='png', bbox_inches="tight", dpi=600)
plt.show()
