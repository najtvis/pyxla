#%% Import libraries
#=======================================================
import matplotlib.pyplot as plt
import numpy as np
import csv
from matplotlib.ticker import (MultipleLocator, AutoMinorLocator)

maxValue = 39.95188
maxValue = 0.0

with open('19keV_offset_0.2.txt', newline='') as csvfile:
    reader = csv.reader(csvfile, delimiter=' ', quotechar='|')
    i=0
    for row in reader:
        i=i+1
        if(i==128):
            row1 = np.zeros((len(row)))
            for j in range(0, len(row)-1): 
                row1[j] = float(row[j])# / maxValue
                if(row1[j] > maxValue):
                    maxValue = row1[j]
  
with open('2keV_offset_0.2.txt', newline='') as csvfile:
    reader = csv.reader(csvfile, delimiter=' ', quotechar='|')
    i=0
    for row in reader:
        i=i+1
        if(i==128):
            row2 = np.zeros((len(row)))
            for j in range(0, len(row)-1): 
                row2[j] = float(row[j])# / maxValue
                if(row2[j] > maxValue):
                    maxValue = row2[j]
   
with open('R100_offset_0.2.txt', newline='') as csvfile:
    reader = csv.reader(csvfile, delimiter=' ', quotechar='|')
    i=0
    for row in reader:
        i=i+1
        if(i==128):
            row3 = np.zeros((len(row)))
            for j in range(0, len(row)-1): 
                row3[j] = float(row[j])# / maxValue
                if(row3[j] > maxValue):
                    maxValue = row3[j]

for j in range(0, len(row1)-1): 
    row1[j] = row1[j] / maxValue

for j in range(0, len(row2)-1): 
    row2[j] = row2[j] / maxValue

for j in range(0, len(row3)-1): 
    row3[j] = row3[j] / maxValue

# x=np.linspace(0,len(row1)-1, len(row1))
x=np.linspace(0, 0.055*256, 256)

size=13

fig, ax = plt.subplots(figsize=(5.5,3.2))
ax.plot(x, row3, label="100 %", linestyle = '-')
ax.plot(x, row2, label="2.13 keV", linestyle = '--')
ax.plot(x, row1, label="19.3 keV", linestyle = ':')
ax.legend(loc='upper left', ncol=1, prop={'size': 0.8*size})
ax.set_xlabel("Detector position (mm)", fontsize=size)
ax.set_ylabel("Normalised intensity (-)", fontsize=size)
ax.set_xticks([0, 2, 4, 6, 8, 10, 12, 14], fontsize=0.8*size)
ax.set_yticks([0, 0.2, 0.4, 0.6, 0.8, 1.0], fontsize=0.8*size)
ax.set_ylim(bottom=0, top=1.05)
ax.grid(b=True, which='major', linestyle='-')
ax.grid(b=True, which='minor', linestyle='--', alpha=0.2)
ax.minorticks_on()

plt.xlim(left=0, right=14)
fig.savefig("comparison_intensity" + '.eps', format='eps', bbox_inches="tight")
fig.savefig("comparison_intensity" + '.png', format='png', bbox_inches="tight", dpi=600)
# plt.show()
# ulozeni zoomu
ax.set_xticks(np.arange(0,14,0.1), fontsize=0.8*size, minor= True)
ax.set_xticks([0, 2, 4, 6, 8, 10, 11, 11.5, 12, 12.5, 13, 14], fontsize=0.8*size)

plt.xlim(left=11, right=13)
fig.savefig("comparison_intensity_zoom" + '.eps', format='eps', bbox_inches="tight")
fig.savefig("comparison_intensity_zoom" + '.png', format='png', bbox_inches="tight", dpi=600)
plt.show()

