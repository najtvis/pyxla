    #%% Import libraries
#=======================================================
import matplotlib.pyplot as plt
import numpy as np
from matplotlib import ticker

import plotFunctions


zemax = np.zeros((256, 256))
optometrika = np.zeros((256, 256))
lesim = np.zeros((512, 512))
pyxla = np.zeros((512, 512))
pyxla2 = np.zeros((256, 256))
pyxla3points = np.zeros((256, 256))

pyxla_19kev = np.zeros((256, 256))
pyxla_19kev_off = np.zeros((256, 256))
pyxla_2kev = np.zeros((256, 256))
pyxla_2kev_off = np.zeros((256, 256))


zemax = plotFunctions.loadCSV('zemax_941_with_thickness_data_only.txt', zemax, '\t', 0)
optometrika = plotFunctions.loadCSV('optometrika_965_data_only.txt', optometrika, '\t', 0)
lesim = plotFunctions.loadCSV('lesim_2kev.txt', lesim, ' ', 1)
pyxla = plotFunctions.loadCSV('2keV_no_adj_0.txt', pyxla, ' ', 1)
pyxla2 = plotFunctions.loadCSV('8keV_2D_uniform_-14.txt', pyxla2, ' ', 1)
pyxla3points = plotFunctions.loadCSV('8keV_1D_3points.txt', pyxla3points, ' ', 1)
pyxla_19kev = plotFunctions.loadCSV('19keV_centre.txt', pyxla_19kev, ' ', 1)
pyxla_19kev_off = plotFunctions.loadCSV('19keV_offset_0.2.txt', pyxla_19kev_off, ' ', 1)
pyxla_2kev = plotFunctions.loadCSV('2keV_centre.txt', pyxla_2kev, ' ', 1)
pyxla_2kev_off = plotFunctions.loadCSV('2keV_offset_0.2.txt', pyxla_2kev_off, ' ', 1)


x256=np.linspace(0, 0.055*256, 256)
x512=np.linspace(0, 0.055*256, 512)

xZemax=np.linspace(-0.11, 0.055*254, 256)
xOptometrika=np.linspace(0, 0.055*256, 256)
xLesim=np.linspace(0.0275*2, 0.0275*(512+2), 512)
xPyxla=np.linspace(0.0275*2.5, 0.0275*(512+2.5), 512)
xPyxla2=np.linspace(0.055, 0.055*(256+1), 256)

zemaxRow = zemax[128]
optometrikaRow = optometrika[128]
lesimRow = lesim[256]
pyxlaRow = pyxla[256]
pyxla2Row = pyxla2[128]
pyxla3pointsRow = pyxla3points[128]

pyxla19kevRow = pyxla_19kev[128]
pyxla19kev_offRow = pyxla_19kev_off[128]
pyxla2kevRow = pyxla_2kev[128]
pyxla2kev_offRow = pyxla_2kev_off[128]

size=10
#fig, ax = plt.subplots(figsize=(4,2.7), dpi=300)
fig, ax = plt.subplots(figsize=(5.5,3.2))

plt.plot(xPyxla, pyxlaRow, label='PyXLA')
plt.plot(xZemax, zemaxRow, label='OpticStudio', linestyle = '--')
plt.plot(xOptometrika, optometrikaRow, label='Optometrika Toolbox', linestyle = '--')
plt.plot(xLesim, lesimRow, label='LOPSIMUL', linestyle = ':')

# plt.plot(xPyxla2, pyxla2Row, label='Pyxla adj')

ax.legend(loc='upper right', bbox_to_anchor=(1, 1), ncol=1, prop={'size': 0.8*size})

plt.xlabel("Detector line (mm)", fontsize=size)
plt.ylabel("Normalised intensity (-)", fontsize=size)
plt.xticks([0, 2, 4, 6, 8, 10, 12, 14], fontsize=0.8*size)
plt.yticks([0, 0.25, 0.5, 0.75, 1], fontsize=0.8*size)

params = {'legend.fontsize': 'large',
          'figure.figsize': (6,3),
          'axes.labelsize': size,
          'axes.titlesize': size,
          'xtick.labelsize': size*0.75,
          'ytick.labelsize': size*0.75,
          'axes.titlepad': 25}
plt.ylim(bottom=0, top=1)
plt.xlim(left=0, right=14)
plt.grid(b=True, which='major', linestyle='-')
plt.minorticks_on()
plt.grid(b=True, which='minor', linestyle='--', alpha=0.2)
fig.savefig('comparison.eps', format='eps', bbox_inches="tight")
fig.savefig('comparison.png', format='png', bbox_inches="tight", dpi=600)
# plt.show()


plotFunctions.plotImage("pyxla", pyxla)
plotFunctions.plotImage("zemax", zemax)
plotFunctions.plotImage("optometrika", optometrika)
plotFunctions.plotImage("lopsimul", lesim)
plotFunctions.plotImage("fig6b", pyxla_19kev)
plotFunctions.plotImage("fig6d", pyxla_19kev_off)
plotFunctions.plotImage("fig6a", pyxla_2kev)
plotFunctions.plotImage("fig6c", pyxla_2kev_off)
