#%% ====================================================
# Import libraries
#=======================================================

from tokenize import String
import numpy as np
import matplotlib.pyplot as plt
from matplotlib import pyplot as rcParams
from matplotlib import cm
from mpl_toolkits.mplot3d import Axes3D
from matplotlib import cm

import src.functions as functions

offset = []
for i in range(-100, -45, 5):
    offset.append(i)

for i in range(-45, 45, 1):
    offset.append(i)

for i in range(45, 101, 5):
    offset.append(i)

fwhmMeas = []
tenthMeas = []
peakMeas = []

energyString = '8keV'
# %% 
for i in offset:
    
    inputFile = 'outputs/8keV_2D_uniform_no_adj_'+str(i)+'.txt'
    image = []
    image = functions.readCSV(inputFile) 
    image = np.array(image)
    x=np.linspace(0, 0.055*256, len(image))

    row = image[int(len(image)/2)]
    [fwhmVal, x1, x2, med_y] = functions.fwhmAllOut(row, x, 0.5)
    maxVal = image.max()
    fwhmVal = fwhmVal * 0.055*256/len(image)
    fwhmMeas.append(fwhmVal)

    [fwhmVal, x1, x2, med_y] = functions.fwhmAllOut(row, x, 0.1)
    maxVal = image.max()
    fwhmVal = fwhmVal * 0.055*256/len(image)
    
    tenthMeas.append(fwhmVal)
    peakMeas.append(maxVal)

    print("Offset:\t%.2f\tFWHM:\t%.4f\tPeak:\t%.2f" % (i, fwhmVal, maxVal))

# %%
maxValue = 0
for j in range(0, len(peakMeas)): 
    if(peakMeas[j] > maxValue):
        maxValue = peakMeas[j]

for j in range(0, len(peakMeas)): 
    peakMeas[j] = peakMeas[j] / maxValue

size=13

fig, ax = plt.subplots(figsize=(5.5,3.2))
ax2 = ax.twinx()
ax.plot(offset,  peakMeas,  '-', color='tab:blue', drawstyle='steps-mid', label='Peak', markersize=5, markeredgecolor='tab:blue', markerfacecolor='none')
ax2.plot(offset, tenthMeas, '+-', color='tab:red', label='FWTM', markersize=4)
ax2.plot(offset, fwhmMeas,  'o-', color='tab:red', label='FWHM', markersize=3)

lines, labels = ax.get_legend_handles_labels()
lines2, labels2 = ax2.get_legend_handles_labels()
ax2.legend(lines + lines2, labels + labels2, loc='upper left', prop={'size': 0.8*size})

ax.set_xlabel("Detector offset (mm)", fontsize=size)
ax.set_ylabel("Normalised intensity (-)", fontsize=size, color='tab:blue')
ax2.set_ylabel("FWHM/FWTM (mm)", fontsize=size, color='tab:red')
ax.tick_params(axis='y', colors='tab:blue')
ax2.tick_params(axis='y', colors='tab:red')

ax.set_xticks([-100, -50, 0, 50, 100], fontsize=0.8*size)
ax.set_yticks([0, 0.2, 0.4, 0.6, 0.8, 1.0], fontsize=0.8*size, color='tab:blue')
ax2.set_yticks([0, 0.04, 0.08, 0.12, 0.16, 0.20, 0.24], fontsize=0.8*size, color='tab:red')

params = {'legend.fontsize': 'large',
          'figure.figsize': (6,3),
          'axes.labelsize': size,
          'axes.titlesize': size,
          'xtick.labelsize': size*0.75,
          'ytick.labelsize': size*0.75,
          'axes.titlepad': 25}
ax.set_ylim(bottom=0, top=1.05)
ax2.set_ylim(bottom=0, top=0.21)

plt.xlim(left=-100, right=100)
ax.grid(b=True, which='major', linestyle='-')
ax.minorticks_on()
ax.grid(b=True, which='minor', linestyle='--', alpha=0.2)

ax.axhline(y = 0.95, color = 'gray', linestyle = '--', linewidth=1)
ax.axvline(x = -29, color = 'gray', linestyle = '--', linewidth=1.25)
ax.axvline(x = 29, color = 'gray', linestyle = '--', linewidth=1.25)

fig.savefig("peak_fwhm" + '.eps', format='eps', bbox_inches="tight")
fig.savefig("peak_fwhm" + '.png', format='png', bbox_inches="tight", dpi=600)
plt.show()

