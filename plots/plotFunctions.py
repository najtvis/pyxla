import matplotlib.pyplot as plt
from matplotlib import ticker
import csv
import numpy as np

def loadCSV(filename, array, delimiter = ' ', transpose = 1):
    with open(filename, newline='') as csvfile:
        maxVal = 0
        reader = csv.reader(csvfile, delimiter=delimiter, quotechar='|')
        i=0
        for row in reader:
            for j in range(0, len(row)-1): 
                array[j-1][i] = float(row[j]) 
                
                if(array[j-1][i] > maxVal):
                    maxVal = array[j-1][i]
            i=i+1
        if transpose:
            array = np.transpose(array)

        for i in range(0, len(array)):
            for j in range(0, len(array[i])): 
                array[i][j] = array[i][j]/maxVal 
    return array



def plotImage(fileName, array):
    size = 13
    fig, ax = plt.subplots(figsize=(5.5,3.2), dpi=300)
    im = ax.imshow(array, cmap='Greys', vmin=0, vmax=1, extent=[0,14,0,14])	
    cbar = fig.colorbar(im, ax=ax)
    formatter = ticker.ScalarFormatter(useMathText=True)
    formatter.set_scientific(True) 
    formatter.set_powerlimits((0,1))
    cbar.ax.yaxis.get_offset_text().set_fontsize(0.8*size)
    cbar.ax.yaxis.set_major_formatter(formatter) 
    cbar.ax.tick_params(labelsize=0.8*size)
    cbar.set_label('Normalised intensity (-)', rotation=90, fontsize=size)
    plt.xlabel("x (mm)", fontsize=size)
    plt.ylabel("y (mm)", fontsize=size)
    plt.xticks([0, 2, 4, 6, 8, 10, 12, 14], fontsize=0.8*size)
    plt.yticks([0, 2, 4, 6, 8, 10, 12, 14], fontsize=0.8*size)
    fig.savefig(fileName + '.eps', format='eps', bbox_inches="tight")
    fig.savefig(fileName + '.png', format='png', bbox_inches="tight", dpi=600)


def plotChart(fileName, x, y, xLabel, yLabel, label, xmin, xmax, xTicks, ymin, ymax, yTicks):
    size = 13
    fig, ax = plt.subplots(figsize=(5.5,3.2))

    plt.plot(x, y, label=label)
    # ax.legend(loc='upper right', bbox_to_anchor=(1, 1), ncol=1, prop={'size': 0.8*size})
    plt.xlabel(xLabel, fontsize=size)
    plt.ylabel(yLabel, fontsize=size)
    plt.xticks(xTicks, fontsize=0.8*size)
    plt.yticks(yTicks, fontsize=0.8*size)

    params = {'legend.fontsize': 'large',
            'figure.figsize': (6,3),
            'axes.labelsize': size,
            'axes.titlesize': size,
            'xtick.labelsize': size*0.75,
            'ytick.labelsize': size*0.75,
            'axes.titlepad': 25}
    plt.ylim(bottom=ymin, top=ymax)
    plt.xlim(left=xmin, right=xmax)
    plt.grid(b=True, which='major', linestyle='-')
    plt.minorticks_on()
    plt.grid(b=True, which='minor', linestyle='--', alpha=0.4)
    fig.savefig(fileName + '.eps', format='eps', bbox_inches="tight")
    fig.savefig(fileName + '.png', format='png', bbox_inches="tight", dpi=600)
    plt.show()