#%% Import libraries
#=======================================================
import matplotlib.pyplot as plt
import numpy as np
import csv

tab1 = np.zeros((3, 501))
tab2 = np.zeros((3, 501))
tab3 = np.zeros((3, 501))
with open('2123ev.txt', newline='') as csvfile:
    reader = csv.reader(csvfile, delimiter=' ', quotechar='|')
    i=0
    for row in reader:
        i=i+1            
        for j in range(0, len(row)-1): 
            tab1[j][i] = float(row[j]) 
  
with open('8048eV.txt', newline='') as csvfile:
    reader = csv.reader(csvfile, delimiter=' ', quotechar='|')
    i=0
    for row in reader:
        i=i+1            
        for j in range(0, len(row)-1): 
            tab2[j][i] = float(row[j])   
   
with open('19279eV.txt', newline='') as csvfile:
    reader = csv.reader(csvfile, delimiter=' ', quotechar='|')
    i=0
    for row in reader:
        i=i+1            
        for j in range(0, len(row)-1): 
            tab3[j][i] = float(row[j]) 
                

size=8
fig  = plt.figure(figsize=(4,2.7), dpi=300)

plt.plot(tab1[0], tab1[1], label="2.13 keV", linestyle = '-')
plt.plot(tab2[0], tab2[1], label="8.05 keV", linestyle = '--')
plt.plot(tab3[0], tab3[1], label="19.3 keV", linestyle = ':')

# plt.xlim((0,2))
# plt.ylim((0,1))

plt.xlabel("x cross-section (px)", fontsize=size)
plt.ylabel("Intensity (-)", fontsize=size)
# plt.xticks([0, 0.2, 0.4, 0.6, 0.8, 1.0, 1.2, 1.4, 1.6, 1.8, 2.0], fontsize=0.8*size)
# plt.yticks([0, 0.2, 0.4, 0.6, 0.8, 1.0], fontsize=0.8*size)
ax = plt.axes()
ax.grid(b=True, which='major', linestyle='--')
# ax.grid(b=True, which='minor', linestyle='--')

plt.minorticks_on()
ax.grid(b=True, which='minor', linestyle='--', alpha=0.2)
ax.legend(frameon=True, fontsize=0.8*size, loc='upper right' )

fig.savefig('reflectivity.eps', format='eps', bbox_inches="tight")
fig.savefig('reflectivity.png', format='png', bbox_inches="tight", dpi=600)
plt.show()