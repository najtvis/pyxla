# %%
import csv
import numpy as np
import matplotlib.pyplot as plt
from matplotlib import pyplot as rcParams
from matplotlib import cm
import pandas as pd


#from PIL import ImageDraw


def readCSV(fileName):
    with open(fileName, 'r') as f:
        print(fileName)
        next(f) #skip line
        df = pd.read_csv(f, delimiter=',')
        #saved_column = df.column_name #you can also use df['column_name']
        #reader = [list(map(float,rec)) for rec in csv.reader(f, delimiter=' ')]
        #image = list(reader)
    #print(df)
    return df

def drawCircle(ax, x, y, r, colour):
    circle = plt.Circle((x, y), r, color=colour, fill=False, lw=0.25)
    ax.add_patch(circle)


threshold = 0


energy=0
# Prepare histograms

size=13
#fig, ax = plt.subplots(figsize=(4,2.7), dpi=300)
fig, ax = plt.subplots(figsize=(5.5,3.2))

name = 'inputs/reflectivity/2123eV'
graph = readCSV(name + '.dat')
plt.plot(graph['Angle (deg)'], graph['Reflectivity'], label='2.13 keV')

name = 'inputs/reflectivity/8048eV'
graph = readCSV(name + '.dat')
plt.plot(graph['Angle (deg)'], graph['Reflectivity'], ':', label='8.05 keV')

name = 'inputs/reflectivity/19279eV'
graph = readCSV(name + '.dat')
plt.plot(graph['Angle (deg)'], graph['Reflectivity'], '--', label='19.3 keV')

name = 'fig4'
#show image
#im = ax.imshow(stp.detector.pix_ar, cmap='Greys', vmin=0, vmax=10)	
# name = 'R_vs_E_at_0.1deg'
# graph = readCSV(name + '.txt')
# plt.plot(graph['Photon Energy (eV)'], graph['Reflectivity'], label='0.10°')

# name = 'R_vs_E_at_0.25deg'
# graph = readCSV(name + '.txt')
# plt.plot(graph['Photon Energy (eV)'], graph['Reflectivity'], label='0.25°')	

# name = 'R_vs_E_at_0.5deg'
# graph = readCSV(name + '.txt')
# plt.plot(graph['Photon Energy (eV)'], graph['Reflectivity'], label='0.50°')	

# name = 'R_vs_E_at_1.0deg'
# graph = readCSV(name + '.txt')
# plt.plot(graph['Photon Energy (eV)'], graph['Reflectivity'], label='1.00°')	

# name = 'R_vs_E'
ax.legend(loc='upper right', bbox_to_anchor=(1, 1), ncol=1, prop={'size': 0.8*size})
# ax.legend(loc='left bottom', prop={'size': 0.8*size})
#colorbar
#cbar = fig.colorbar(im, ax=ax)
#cbar.ax.set_yticks([0, 10, 20, 30, 40])
#cbar.ax.tick_params(labelsize=0.8*size)
#cbar.set_label('Intensity (-)', rotation=90, fontsize=size)

plt.xlabel("Angle (deg.)", fontsize=size)
plt.ylabel("Reflectivity (-)", fontsize=size)
plt.xticks(np.arange(0, 2.01, 0.25), fontsize=0.8*size)
plt.yticks([0, 0.25, 0.5, 0.75, 1], fontsize=0.8*size)

params = {'legend.fontsize': 'large',
          'figure.figsize': (6,3),
          'axes.labelsize': size,
          'axes.titlesize': size,
          'xtick.labelsize': size*0.75,
          'ytick.labelsize': size*0.75,
          'axes.titlepad': 25}
plt.ylim(bottom=0, top=1)
plt.xlim(left=0, right=2)
plt.grid(b=True, which='major', linestyle='-')
plt.minorticks_on()
plt.grid(b=True, which='minor', linestyle='--', alpha=0.2)
fig.savefig(name + '.eps', format='eps', bbox_inches="tight")
fig.savefig(name + '.png', format='png', bbox_inches="tight", dpi=600)
plt.show()
# %%
