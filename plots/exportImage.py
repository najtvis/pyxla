import csv
import numpy as np
import matplotlib.pyplot as plt
from matplotlib import pyplot as rcParams
from matplotlib import cm
#from PIL import ImageDraw


def readCSV(fileName):
    with open(fileName, 'r') as f:
        #next(f) #skip first line
        reader = [list(map(float,rec)) for rec in csv.reader(f, delimiter=' ')]
        image = list(reader)
    return image

def drawCircle(ax, x, y, r, colour):
    circle = plt.Circle((x, y), r, color=colour, fill=False, lw=0.25)
    ax.add_patch(circle)

name = '8keV_1D_uniform'
threshold = 0
matrixArray = readCSV("outputs/" + name + '.txt')

energy=0
# Prepare histograms

size=8
#fig, ax = plt.subplots(figsize=(4,2.7), dpi=300)
fig, ax = plt.subplots(figsize=(4,2.7))
#show image
#im = ax.imshow(stp.detector.pix_ar, cmap='Greys', vmin=0, vmax=10)	
im = ax.imshow(matrixArray, cmap='Greys')	
#colorbar
cbar = fig.colorbar(im, ax=ax)
cbar.ax.set_yticks([0, 10, 20, 30, 40])
cbar.ax.tick_params(labelsize=0.8*size)
cbar.set_label('Intensity (-)', rotation=90, fontsize=size)

plt.xlabel("x (px)", fontsize=size)
plt.ylabel("y (px)", fontsize=size)
plt.xticks([0, 50, 100, 150, 200, 250], fontsize=0.8*size)
plt.yticks([0, 50, 100, 150, 200, 250], fontsize=0.8*size)

params = {'legend.fontsize': 'large',
          'figure.figsize': (4,2.7),
          'axes.labelsize': size,
          'axes.titlesize': size,
          'xtick.labelsize': size*0.75,
          'ytick.labelsize': size*0.75,
          'axes.titlepad': 25}

points = []
for idx in range(0, len(matrixArray)) : 
    for idy in range(0, len(matrixArray[idx])) : 
        if matrixArray[idx][idy] > 0: 
            points.append([idx, idy]) 

print(points)

#for point in range(0, len(points)):
#    drawCircle(ax, points[point][1], points[point][0], 4, 'r')


fig.savefig(name + '.eps', format='eps', bbox_inches="tight")
fig.savefig(name + '.png', format='png', dpi=600, bbox_inches="tight")
plt.show()