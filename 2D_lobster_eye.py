#%% ====================================================
# Import libraries
#=======================================================
import src.setup3D as setup3D
import src.ray as ray
import src.objects as objects
import src.functions as functions

import math
import numpy as np
import time
import matplotlib.pyplot as plt
from matplotlib import pyplot as rcParams
from matplotlib import cm


#%% ====================================================
# 1, Initial setup
#=======================================================
tStart = time.time()

# Check if output directory exists, if not, create it
functions.createDir("outputs")

# set font size for plots
size=8

# Decision if the 3D will be printed out and includes rays
# Do not print out more than 1000 rays, otherwise it consumes too much RAM!
# True = yes, False = no
draw3D   = False
drawRays = False

#%% ====================================================
# 1.1, set detector
#=======================================================
# set detector type timepix, timepix512, timepix_div_10, tpx_quad
# in case of different type of detector, add it in file objects.py
detectorType = 'timepix'   

#%% ====================================================
# 1.2, set source
#=======================================================

# all spatial dimensions are in milimeters
# resolution in H and V axis
# 1 means full, 2 means half, ...
# requires to be 
divH = 1
divV = 1

divXmm = 0.0275           # set pitch between rayx
divYmm = 0.0275

noPoints = (1000, 1000)      #set number of point in x,y

angleX = 0.000000000    # in degrees
angleY = 0.000000000    # in degrees

# Define input points
focusPoint = np.array([0.00001, 0.00001, 0.00001])     # detector position 
detectorOffset = np.array([0,0,0])
vStackOffset = np.array([0,0,0])
hStackOffset = np.array([0,0,0])

#%% ====================================================
# 2, create mirror modules stacks
#=======================================================
mirrors = []

vStack  = objects.createMirrorModule(47, 150, 75, 0.35, 0.75, True, 'V', focusPoint + vStackOffset, 965*2)
hStack  = objects.createMirrorModule(47, 150, 75, 0.35, 0.75, True, 'H', focusPoint + vStackOffset, 965*2)
aperturePoint = np.array([-vStack.focusLength - vStack.mDepth/2, 0, 0])
mirrors.append(vStack)
mirrors.append(hStack)

#%% ====================================================
# 3, create detector
#=======================================================

det = objects.createDetector(focusPoint + detectorOffset, detectorType)

#%% ====================================================
# 4, create setup and plot it is allowed
#=======================================================

# creates a setup object with given mirrors and detector, numerical
# parameters define the size of the 3D plot space, and the last param
# controls whether this plot is shown or not
stp = setup3D.Setup3D(np.array(mirrors), det, drawRays)

if draw3D:
    stp.plot_setup([-2000, 5], [-60, 60], [-60, 60])
    # plt.show()

tEnd = time.time()
print("Execution time for setup: %.2f s" % (tEnd-tStart))

#%% ====================================================
# 5, set energies
#=======================================================

# define the energies of the sources used in the setup
# - first param sets energy in eV
# - second param contains reflectivity data: 
#	- if numeric, then the same value is used for all angles of incidence 
# 	- if string, then treated as a filename for text data with format 
#	  according to [http://henke.lbl.gov/optical_constants/layer2.html]
# stp.appendEnergy(0, 1) # energy no. 1, independent on angle (visible light) 
# stp.appendEnergy(1487, "./inputs/reflectivity/1487eV.dat") #  0...5 degs
# stp.appendEnergy(2123, "./inputs/reflectivity/2123eV.dat") #  0...5 degs
stp.appendEnergy(8048, "./inputs/reflectivity/8048eV.dat") #  0...2 degs
# stp.appendEnergy(19279, "./inputs/reflectivity/19279eV.dat") #  0...2 degs

# set the number of channels for the detector to the number of energies used
numEnergies = len(stp.energies)
det.setChannels(numEnergies)  

#%% ====================================================
# 6, create radiation sources
#=======================================================

# define center and direction of the source 
inputAperture = (noPoints[0]*divXmm, noPoints[1]*divYmm)
srcZposition = -1e12
srcPosition = np.array([srcZposition, ((srcZposition + aperturePoint[0]) * math.tan(math.radians(angleY))), ((srcZposition + aperturePoint[0]) * math.tan(math.radians(angleX)))]) 

# calculate direction vector
direct = aperturePoint - srcPosition
direct = np.array(direct / np.linalg.norm(direct))

# create the source given the center, direction and number of mirror stacks
src1 = ray.RaySourceParallel(srcPosition, direct, stp.mirrorStacks.size)

print("Aperture point:")
print(aperturePoint) 
print("Source position:")
print(srcPosition)
print("Direction vector")
print(direct)

# initialise all intensities to 1 (all rays emit all energies with unit intensity)
inten = np.ones(numEnergies)

#%% ====================================================
# 7, run simulation
#=======================================================

src1.processRectGrid([0, 1, 0], inputAperture, ray.SourceType.SOURCE_UNIFORM, (int(noPoints[0]/divH), int(noPoints[1]/divV)), inten, stp) 

#%% ====================================================
# 8, plot results
#=======================================================

# show execution time
tEnd = time.time()
print("Execution time: %.2f s" % (tEnd-tStart))

# save arrangement if is intened to save it
if(draw3D) :
    stp.pltr.fig.savefig('outputs/arrangement.png', format='png', bbox_inches="tight",  dpi=600)
    plt.show()

x=np.linspace(1,stp.detector.pix_x_no, num=stp.detector.pix_x_no)
energy=0
# Prepare histograms

np.savetxt('8keV_1D_3points.txt',stp.detector.pix_ar,fmt='%.5f')

row = stp.detector.pix_ar[int(stp.detector.pix_x_no/2)]
fwhmVal = functions.fwhm(row, x)
maxVal = stp.detector.pix_ar.max()

print("FWHM: %.2f px\nPeak: %.2f" % (fwhmVal, maxVal))

fig = plt.figure()
# plot image
plt.subplot(221)
plt.imshow(stp.detector.pix_ar)	
plt.title("Detector image for E = %d eV" % (stp.energies[0]))
plt.xlabel("x [px]")
plt.ylabel("y [px]")

# plot histogram along y axis
plt.subplot(222)
arr = np.array(stp.detector.pix_ar).T
plt.plot(arr[int(stp.detector.pix_y_no/2)], x)

# plot histogram along x axis
plt.subplot(223)
plt.plot(x, stp.detector.pix_ar[int(stp.detector.pix_x_no/2)])

x=range(stp.detector.pix_x_no)
y=range(stp.detector.pix_y_no)
X, Y = np.meshgrid(x, y)

# plot image in 3D
ax = fig.add_subplot(2, 2, 4, projection='3d')
ax.plot_surface(X, Y, stp.detector.pix_ar, cmap=cm.coolwarm, rstride=1, cstride=1)
plt.show()

# %%
