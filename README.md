# Python X-ray-tracing for Lobster-eye Application

Simulation SW for Lobster - eye optics
Originally developed by students Jozef Dujava and Marek Jaluvka at Czech Technical University in Prague during their course Space Engineering. 

Tested with Python 3.8.10 on Oracle Linux 8.5 and on Windows 10/11.

## Installation

At first select operating system which will be PyXLA installed and after that perform general installation for the Python 3

### Oracle Linux 8.5 / CentOS 7/8

Prerequsities:
```
yum install -y python3 python3-devel python3-tkinter
pip3 install --user -r requirements.txt
```
### Ubuntu 20.04 LTS

Prerequsities:
```
apt-get install -y python3 python3-dev python3-tk
pip3 install --user -r requirements.txt
```

### Windows

Prerequsities:
Install MS C++ 2014 or greater from here https://visualstudio.microsoft.com/visual-cpp-build-tools/

``` 
Python at least version 3.8 
```

In the Python environment install required packages for windows as following
```
pip3 install --user -r requirements.win.txt
```

## Usage

### Source files

PyXLA consists of several source files located in the `src` folder. In the following table is a short description of the files.

| File                             | Description                                                        |
|----------------------------------|--------------------------------------------------------------------|
| [detector.py](src/detector.py)   | Class of detector defined in 3D space                              |
| [functions.py](src/functions.py) | Commonly used function for reading and evaluataion                 |
| [mirror.py](src/mirror.py)       | Definition of each mirror and Mirror Stack which creates an optics |
| [objects.py](src/objects.py)     | Definition of detectors types and optics                           |
| [plot3d.py](src/plot3d.py)       | Class for definition of 3D space for simulation                    |
| [ray.py](src/ray.py)             | Classes for definition of rays and their properties                |
| [setup3D.py](src/setup3D.py)     | Class for processing rays and behaviours in the 3D space           |

### Examples

Several examples are provided for verification and testing purposes. 

#### 1D Lobster-eye optics

An example provides a simulation of 1D Lobster-eye optics with a focal length of 965 mm and a Timepix detector. As a source is selected an 8 keV point source located in the infinity, represented as parallel rays incoming on the input aperture. These rays are reflected from surfaces and go to the focal point on the detector. 

Detailed description is in [1D_lobster_eye.ipynb](1D_lobster_eye.ipynb) file and source code in [1D_lobster_eye.py](1D_lobster_eye.py) source file.

#### 2D Lobster-eye optics

An example of 2D Lobster-eye optics represents two identical sets of 1D optics with the same focal length of 965 mm and creates Angel's type of optics. As a detector is chosen a Timepix detector with a 256 x 256 matrix array. 

Source code is located in [2D_lobster_eye.py](2D_lobster_eye.py) file.

#### Focal length verification

The focal length verification example represents the determination of the behaviour of the optics while the detector is moved from its designed position rear and back. Designed optics is chosen as in the previous examples with a focal length of 965 mm and Timepix detector.

 Source code is located in [z_axis_scan.py](z_axis_scan.py) file.
