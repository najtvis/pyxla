import math
import numpy as np

class Detector:    
	# defined by two points and number of pixels
	def __init__(self, p1, p2, pix_no): 
		"""
		Define a detector in 2D space

		Args:
			p1 (array): 1st coordinate in space, require 2D array
			p2 (array): 2nd coordinate in space, require 2D array
			pix_no (int): number of pixels
		"""
		self.p1 = p1
		self.p2 = p2
		self.length = math.sqrt((p2[0] - p1[0])**2 + (p2[1] - p1[1])**2)
		self.angle = math.atan2(p2[1] - p1[1], p2[0] - p1[0])
		self.u = (p2[0] - p1[0], p2[1] - p1[1]) # direction vektor
		self.pix_no = pix_no
		self.pix_ar = np.zeros(pix_no)
		self.firstImage = True

class Detector3D:
	# defined by three points to form pallarelogram and number of pixels in x and y direction
	def __init__(self, center, width, height, pix_x_no, pix_y_no): 
		"""
		Define a detector physical dimensions

		Args:
			center (float): center of the rectangle
			width (float): width of detector
			height (float): height of detector
			pix_x_no (int): number of pixels in x axis
			pix_y_no (int): number of pixels in y axis
		"""
		
		self.A = center + width/2 - height/2 # corner A
		self.B = center - width/2 - height/2 # corner B
		self.C = center - width/2 + height/2 # corner C
		self.D = center + width/2 + height/2 # corner D
		
		self.center = center
		
		self.v = self.A - self.B
		self.w = self.C - self.B
		self.n = -np.cross(self.v, self.w)
		
		self.pix_x_no = pix_x_no
		self.pix_y_no = pix_y_no
		self.firstImage = True
		
	def setChannels(self, numChannels):
		"""
		Define number of energy channels

		Args:
			numChannels (int): number of energy channels
		"""
		self.pix_ar = np.zeros((self.pix_x_no, self.pix_y_no, numChannels))
		

	
 

