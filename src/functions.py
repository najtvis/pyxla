import numpy as np
import os
import pandas as pd

def createDir(dir):
    """
    Create a folder if not exists

    Args:
        dir (string): name of folder
    """
    CHECK_FOLDER = os.path.isdir(dir)

    # If folder doesn't exist, then create it.
    if not CHECK_FOLDER:
        os.makedirs(dir)

def readCSV(fileName):
    """
    Function for reading a CSV file

    Args:
        fileName (string): path to file

    Returns:
        array: array of data
    """
    with open(fileName, 'r') as f:
        df = pd.read_csv(f, sep='\s+', index_col=None, header=None)
    return df

def lin_interp(x, y, i, level):
    """
    Compute linear interpolation of data

    Args:
        x (array): array of x values
        y (array): array of y values
        i (int): index of y array
        level (_type_): value of height 

    Returns:
        float: interpolated value at index i
    """
    return x[i] + (x[i+1] - x[i]) * ((level - y[i]) / (y[i+1] - y[i]))

def fwhmAllOut(yList, xList, level=0.5):
    """
    Compute FWHM (default) of the function 
    If level value is not redefined, it is at 0.5 of max

    Args:
        yList (array): values for y axis
        xList (array): values for x axis
        level (float, optional): set level where width is computed (range is 0-1). Defaults to 0.5.

    Returns:
        list: list of values regarding to FWHM

    """
    xList = xList
    yList = np.array(yList)
    
    y_min = (min(yList))
    y_max = (max(yList))
            
    med_y = ((y_max-y_min)*level)
    fwhm_index = int(np.where(yList == y_max)[0][0])
    
   
    signs = np.sign(np.add(yList, -med_y))
    zero_crossings = (signs[0:-2] != signs[1:-1])
    zero_crossings_i = np.where(zero_crossings)[0]

    fwhmLeft = 0
    fwhmRight = 0
    # if(zero_crossings_i != None):
    if(len(zero_crossings) == 2):
        fwhmLeft = zero_crossings_i[0]
        fwhmRight = zero_crossings_i[1]
    else:
        
        fwhmRight = zero_crossings_i[np.where(zero_crossings_i > fwhm_index)].min()
        fwhmLeft = zero_crossings_i[np.where(zero_crossings_i < fwhm_index)].max()

    x1 = lin_interp(xList, yList, fwhmRight, med_y)
    x2 = lin_interp(xList, yList, fwhmLeft, med_y)
    return (abs(x1-x2), x1, x2, med_y)
    # else:
    #     return (0,0,0,0)

def fwhm(yList, xList, level=0.5):
    """
    Compute FWHM of the function

    Args:
        yList (array): values for y axis
        xList (array): values for x axis
        level (float, optional): set level where width is computed (range is 0-1). Defaults to 0.5.

    Returns:
        float: width of peak
    """
    [fwhmVal, x1, x2, med_y] = fwhmAllOut(yList, xList, level)
    return (fwhmVal)