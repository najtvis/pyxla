import math
import matplotlib.pyplot as plt
import numpy as np
from matplotlib import cm
import src.plot3d as plot3d
import src.mirror as mirror
import colorsys
import multiprocessing
import copy

colorDirect 	= '#cc3d3d'
colorReflected	= '#94630d'
colorReflected2	= '#81b013'
colorMirror     = '#0394fc'

class Setup3D:
	def __init__(self, mirrorStacks, detector, plotRays = 0):
		self.rays = []
		self.mirrorStacks = mirrorStacks
		self.detector = detector
		self.energies = []
		self.energyReflectivity = np.array([])
		self.intensityCutoff = 0
		self.plotRay = plotRays
		self.noCPUs = multiprocessing.cpu_count()

		
		
		# mCountAll = 0
		# for j in range(len(mirrorStacks)):
		# 	mCountAll = mCountAll + mirrorStacks[j].mirrors.size
		# self.mirrorStack = mirror.MirrorStack(mCountAll,0,0,0,0,0)
		# # mirrorStack = copy.deepcopy(mirrorStacks[0])
		# self.mirrorStack.mCount = mCountAll
		# self.mirrorStack.invTransMat = mirrorStacks[0].invTransMat
		# self.mirrorStack.transMat = mirrorStacks[0].transMat
		# self.mirrorStack.centerPoint = mirrorStacks[0].centerPoint
		# self.mirrorStack.radius = mirrorStacks[0].radius
		# # self.mirrorStack.thMin = mirrorStacks[0].thMin
		# # self.mirrorStack.thMax = mirrorStacks[0].thMax
		# # self.mirrorStack.dTh = mirrorStacks[0].dTh
		# # self.mirrorStack.thMax = mirrorStacks[0].thMax
		# # self.mirrorStack.thMax = mirrorStacks[0].thMax
		# # self.mirrorStack.centerPoint = mirrorStacks[0].centerPoint
		# self.mirrorStack.mirrors = np.empty(mCountAll, mirror.Mirror3D) # array of mirrors for this stack (Mirror3D)

		# currentMirror = 0
		# for j in range(len(mirrorStacks)):
		# 	currentStack = mirrorStacks[j]
		# 	for i in range(mirrorStacks[j].mirrors.size):
		# 		self.mirrorStack.mirrors[currentMirror] = copy.deepcopy(mirrorStacks[j].mirrors[i])
		# 		currentMirror = currentMirror + 1
		
		# self.mirrorStacks = []
		# self.mirrorStacks.append(self.mirrorStack)
		# self.mirrorStacks = np.array(self.mirrorStacks)
		# test = 0
		
		# print("Number of mirrors: ", mCountAll)
	
	def plot_setup(self, x_size, y_size, z_size, azim=-135, elev=20):		
		self.pltr = plot3d.Plotter3D(x_size, y_size, z_size, azim, elev)
		for i in range(len(self.mirrorStacks)):
			mirrors = self.mirrorStacks[i].mirrors
			for j in range(mirrors.size):
				self.pltr.plot_rectangle(mirrors[j].A, mirrors[j].B, mirrors[j].C, mirrors[j].D , colorMirror)

		self.pltr.plot_rectangle(self.detector.A, self.detector.B, self.detector.C, self.detector.D, (0.0, 0.0, 0.0) )
 	
	def plot_ray(self, ray):
		t_min = np.inf			
		mir = None 
		mirr = None
		orientMatch = False
		imir = -1
		inextmir = -1
		
		for j in range(len(self.mirrorStacks)):
			t = np.inf
			iLast = ray.lastMirror[j]
			iNext = ray.nextMirror[j]
			# if ray has already bounced on a mirror from this stack
			if iLast != None:				
				# and it's sandwiched between mirrors
				if  iNext != None:					
					mir = self.mirrorStacks[j].mirrors[iNext]

					(t, orMatch) = mirror.ray_mirror_intersection(ray, mir, iNext-iLast)
										
					# bounce it back if it hits the next one
					if t != -1 and t < np.inf:
						imir = iNext
						inextmir = iLast
						
					# or stop
					else:
						nextmir = -1
			
			# if it has not bounced, find its first intersect with this stack
			else:
				(t, orMatch, imir, inextmir) = self.mirrorStacks[j].findIntersect(ray)				
				mir = self.mirrorStacks[j].mirrors[imir]
			
			# ray hit the closest mirror
			if t < t_min:
				ray.lastMirror[j] = imir
				ray.nextMirror[j] = inextmir
				t_min = t
				mirr = mir
				orientMatch = orMatch
			# ray hit a further mirror or none at all
			else:
				pass
							
		# did the ray hit the detector before it hit a mirror?
		(t,s,r) = self.ray_detector_intersection(ray, self.detector)
		if t != -1 and t < t_min: # got to detector
			if (self.plotRay):
				endpt = ray.enter_point + ray.u*t
				if(ray.numberReflections == 0):
					self.pltr.plot_line_seg(ray.enter_point,endpt, colorDirect, 'min', 0.3)
				elif(ray.numberReflections == 1):
					self.pltr.plot_line_seg(ray.enter_point,endpt, colorReflected, 'max', 1.0)	
				else:
					self.pltr.plot_line_seg(ray.enter_point,endpt, colorReflected2, 'max', 1.0)	
						
			dw = self.detector.pix_x_no
			dh = self.detector.pix_y_no
					
			hx = int(np.floor(s*dw))
			hy = int(np.floor(r*dh))
			
			pixel = (ray.intensity, hx, hy)
			# self.detector.pix_ar[hx][hy] += ray.intensity
			return pixel # if yes, end of life for this ray, return
			
		# if detector wasn't hit first...
		if t_min == np.inf: # no intersection with mirror
#               print("No intersection.")
			if (self.plotRay): 
				endpt = ray.u
				if(ray.numberReflections == 0):
					self.pltr.plot_line_seg(ray.enter_point,endpt, colorDirect, 'min', 0.3)
				elif(ray.numberReflections == 1):
					self.pltr.plot_line_seg(ray.enter_point,endpt, colorReflected, 'max', 1.0)	
				else:
					self.pltr.plot_line_seg(ray.enter_point,endpt, colorReflected2, 'max', 1.0)	
			
		else:
			inter_pt = ray.enter_point + ray.u * t_min
			if (self.plotRay):
				if(ray.numberReflections == 0):
					self.pltr.plot_line_seg(ray.enter_point,inter_pt, colorDirect, 'min', 0.3)
				elif(ray.numberReflections == 1):
					self.pltr.plot_line_seg(ray.enter_point,inter_pt, colorReflected, 'max', 1.0)	
				else:
					self.pltr.plot_line_seg(ray.enter_point,inter_pt, colorReflected2, 'max', 1.0)	

			# if ray hit the reflective side of the mirror, reflect it
			if orientMatch:
				u = np.cross(mirr.n, np.cross(ray.u, mirr.n))
				cor = self.vector_direction_resolution(u, inter_pt, ray.enter_point)
				norm = np.linalg.norm(u)
				u = u*cor/norm
				if (self.plotRay):
					self.pltr.plot_line_seg(inter_pt, inter_pt + u,(1.0,0.0,0.0))

				n = mirr.n
				cor = self.vector_direction_resolution(n, inter_pt, ray.enter_point)
				norm  = np.linalg.norm(n)
				n = -n*cor/norm

				(x,y) = self.point_coords_in_plane(u, n, ray.enter_point, inter_pt)
				gamma = np.arctan2(y,x)
				beta = np.pi - gamma
				u_out = np.cos(beta)*u + np.sin(beta)*n

				# intensity change due to reflectivity at angle of incidence
				dAngle = np.pi/(2*self.energyReflectivity.shape[0])
				iAngle = int(np.floor(beta/dAngle))

				reflIntensity = ray.intensity * self.energyReflectivity[iAngle]			
				#transIntensity = ray.intensity * self.energyTransmisivity[:, iAngle]
				uOld = ray.u
			
				# REFLECTED RAY			
				if np.linalg.norm(reflIntensity) > self.intensityCutoff:
					ray.enter_point = np.array(inter_pt)
					ray.u = np.array(u_out)
					ray.intensity = reflIntensity	
					ray.numberReflections = ray.numberReflections+1						
					return self.plot_ray(ray)	

		return (0, 0, 0)	


	def ray_detector_intersection(self, ray_in, det):
		A = np.vstack((ray_in.u, det.v, det.w)).transpose()
		#print(np.linalg.matrix_rank(A)) 
		if (np.linalg.matrix_rank(A) < 3): # paprsek je kolineární s rovinou  
			return (-1,-1,-1)
		b = ray_in.enter_point - det.B
		x = np.linalg.solve(A,b)

		t = -x[0]
		s = x[1]
		r = x[2] 

		if s <= 0 or s >= 1 or r <= 0 or r >= 1 or t <= 0 : # paprsek neptrotnul zrcadlo
			return (-1,-1,-1)
		
		
		return (t,s,r)

	def compute_incidence_plane(self, u, n, P): # u - směrový vektor paprsku, n - normálový vektor roviny zrcadla, P - průsečík zrcadla s paprskem
		
		## rovina určená u, n a P
		
		m = np.cross(list(u), n) # normálový vektor kolmé roviny, ax + by + cz + d = 0

		a = m[0]
		b = m[1]
		c = m[2]
		d = -a*P[0] -b*P[1] - c*P[2]

		return (a,b,c,d)

	def compute_plane_intersection(self, n, m): # n, m - normálové vektory rovin
		u = np.cross(list(n), list(m))
		return u
		
	def vector_direction_resolution(self, u, P, enter_point): # u - směrový vektor průsečnice rovin, P - průsečík paprsku s rovinou
		(a,b,c,d) = (u[0], u[1], u[2], -u[0]*P[0] - u[1]*P[1] - u[2]*P[2])
		test_point = (P[0] + u[0], P[1] + u[1], P[2] + u[2])
		if (a*enter_point[0] + b*enter_point[1] + c*enter_point[2] + d)*(a*test_point[0] + b*test_point[1] + c*test_point[2] + d) < 0: # body jsou v rozdílných poloprostorech (u,P)
			return 1
		else: # jsou ve stejném poloprostoru - je  třeba korekce
			return -1

	def point_coords_in_plane(self, u,n, enter_point, P): # u, n - ortonormální báze roviny, zbytek jako u compute_incidence_plane
		A = np.array([[u[0], n[0]],[u[1], n[1]]])
		b = np.array([[enter_point[0]-P[0]],[enter_point[1]-P[1]]])
		if (np.linalg.matrix_rank(A) < 2):
			A = np.array([[u[0], n[0]],[u[2], n[2]]])
			b = np.array([[enter_point[0]-P[0]],[enter_point[2]-P[2]]])
			if (np.linalg.matrix_rank(A) < 2):
				A = np.array([[u[1], n[1]],[u[2], n[2]]])
				b = np.array([[enter_point[1]-P[1]],[enter_point[2]-P[2]]])

		X = np.linalg.solve(A,b)
		x = X[0]
		y = X[1]

		return (x,y)

	# add a source energy. energy in eV	
	def appendEnergy(self, energy, refData):
		if not (type(refData) == str):
			reflectivity = np.ones(100)*refData
			#reflectivity = np.reshape(reflectivity, (1, -1))
			angles = np.linspace(0, 90, 100)
		elif (type(refData) == list):
			angles = refData[0, :]
			reflectivity = refData[1,:]			
		else:
			refls = []
			angls = []
			f = open(refData, "r")	
			# eat the data header
			f.readline()
			f.readline()

			ln = f.readline()
			while ln:
				angle = float(ln.split(',')[0])
				refl = float(ln.split(',')[1])

				refls.append(refl)
				angls.append(angle)
				ln = f.readline()
			f.close()

			# if the input data do not include angles from 0 to 90 degs. add the rest of them with the last reflectivity
			if(angls[len(angls)-1] < 90.0):
				diffAngle = (angls[len(angls)-1] - angls[0]) / len(angls)
				angle = angls[len(angls)-1]
				ref = refls[len(refls)-1]
				while (angle < 90):
					angle += diffAngle
					angls.append(angle)
					refls.append(ref)

			reflectivity = np.array(refls)
			angles = np.array(angls)
		
		if len(self.energies) == 0:
			self.energyReflectivity = reflectivity
			self.energyAngles = angles
		else:
			self.energyReflectivity = np.vstack((self.energyReflectivity, reflectivity))
			self.energyAngles = np.vstack((self.energyAngles, angles))
		self.energies.append(energy)

	def getDetectorImage(self):
		hue = 0.666
		sat = 1
		lum = 0.5
		
		pixData = self.detector.pix_ar[:, :] # pick which channel to plot
		
		maxval = np.amax(pixData)
		if maxval == 0:
			maxval = 1
		print("Max pixel value: %.2f" % maxval, flush=True)
		
		width = self.detector.pix_x_no
		height = self.detector.pix_y_no
		
		screen = np.zeros((height, width, 3), dtype = np.uint8)
		for i in range(width):
			for j in range(height):
				val = pixData[i][j]/maxval
				(r, g, b) = colorsys.hls_to_rgb(0.5 * val + hue, lum, sat)
				screen[j][i][0] = r*255
				screen[j][i][1] = g*255
				screen[j][i][2] = b*255
		
		return screen

	def plot_detector(self, ticks, interpol="none"):
		screen = self.getDetectorImage()
        
		width = self.detector.pix_x_no
		height = self.detector.pix_y_no
		
		plt.imshow(screen, interpolation = interpol, extent=[0, width, height,0])		
		plt.title("Detector image for E = %d eV" % (self.energies[0]))
		plt.xlabel("x [px]")
		plt.ylabel("y [px]")
		
		if ticks !=0:
			xr = range(0, width + 1, ticks)
			plt.xticks(xr)
			plt.yticks(xr)
			plt.grid(True)
		plt.show()
		
		return screen		
		
	def plot_histo(self, axis, ticks):		
		pixData = self.detector.pix_ar[:, :] # pick which channel to plot
		
		width = self.detector.pix_x_no
		height = self.detector.pix_y_no
					
		if (axis == "x"):
			xr = range(0, width+1, ticks)
			screen = np.zeros(width)
			for i in range(width):
				for j in range(height):	
					screen[i] += pixData[i][j]
		else:
			xr = range(0, height+1, ticks)
			screen = np.zeros(height)
			for i in range(height):
				for j in range(width):	
					screen[i] += pixData[j][i]
			
		maxval = np.amax(screen)
		if maxval == 0:
			maxval = 1
		screen = screen/maxval	
			
		plt.plot(screen)
		plt.title("Histogram along %s-axis for E = %d eV" % (axis, self.energies[0]))
		plt.xlabel("%s [px]" % (axis))
		plt.ylabel("Intensity [-]")
		plt.xticks(xr)
		#plt.yticks(yr)
		plt.grid(True)		
		plt.show()
		
		return screen
		

