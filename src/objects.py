import numpy as np
import src.mirror as mirror
import src.detector as detector

def createMirrorModule(mCount, mDepth, mWidth, mThick, mSpacing, doubleSided, type, focusPoint, radius):
    """ 
    Create stack of mirrors according to radius of curvature and place to position focusPoint.

    Args:
        mCount (int): number of mirrors
        mDepth (float): length of mirror (depth)
        mWidth (float): width of mirror
        mThick (float): thickness of mirror
        mSpacing (float): spacing between mirrors
        doubleSided (bool): is mirrors double-sided (true) or only face toward to centre (false)
        type (string): orientation of the module (H, V, ...)
        focusPoint (float): position of focus point
        radius (float): radius of curvature

    Returns:
        mirrorStack: object - complete module
    """
    stack = mirror.MirrorStack(mCount, mDepth, mWidth, mThick, mSpacing, doubleSided)
    
    if(type == 'V'):
        stack.setOrientation(np.array([-1, 0, 0]), np.array([0,0,1]))
    elif(type == 'H'):
        stack.setOrientation(np.array([-1, 0, 0]), np.array([0,1,0]))
    elif(type == '+45'):
        stack.setOrientation(np.array([-1, 0, 0]), np.array([0,1,1]))
    elif(type == '-45'):
        stack.setOrientation(np.array([-1, 0, 0]), np.array([0,1,-1]))
    # else:
    #     raise Exception('Use (H)orizontal or (V)ertical orientation')

    # define the curvature and placement of mirrors for the stack
    stack.setFocus(focusPoint, radius) # either by giving the focus point and distance

    # generate the mirrors once focal point + distance or center + radius were set
    stack.genMirrors()
    return stack

def createDetector(focusPoint, type):
    """
    Select a detector which will be used for simulation or add here new one

    Args:
        focusPoint (3D array): position of the detector, should be focal point
        type (string): select from predefined detectors

    Raises:
        Exception: if not selected from predefined detectors

    Returns:
        detector: object of detector plane
    """
    # set center of detector
    detCenter = focusPoint + np.array([0, 0, 0]) 

    # set the orientation of detector
    uW = np.array([0, -1, 0]) # width vector
    uH = np.array([0, 0, -1]) # height vector

    if (type == 'timepix'):
        # set Width and Height in pixels (resolution)
        detWpx = 256
        detHpx = 256

        # set Width and Height in milimeters
        detWmm = 14.08
        detHmm = 14.08

        # vectors defining detector orientation and size in mm
        detW = detWmm * uW
        detH = detHmm * uH
    elif (type == 'timepix512'):
        # set Width and Height in pixels (resolution)
        detWpx = 512
        detHpx = 512

        # set Width and Height in milimeters
        detWmm = 14.08
        detHmm = 14.08

        # vectors defining detector orientation and size in mm
        detW = detWmm * uW
        detH = detHmm * uH
    elif (type == 'timepix_div_10'):
        # set Width and Height in pixels (resolution)
        detWpx = 2560
        detHpx = 2560

        # set Width and Height in milimeters
        detWmm = 14.08
        detHmm = 14.08

        # vectors defining detector orientation and size in mm
        detW = detWmm * uW
        detH = detHmm * uH
    elif (type == 'tpx_quad'):
        # set Width and Height in pixels (resolution)
        detWpx = 512
        detHpx = 512

        # set Width and Height in milimeters
        detWmm = 28.16
        detHmm = 28.16

        # vectors defining detector orientation and size in mm
        detW = detWmm * uW
        detH = detHmm * uH
    else:
        raise Exception('Allowed detectors are: timepix, timepix512, tpx_quad, timepix_div_10')
    
    # creates the detector
    det = detector.Detector3D(detCenter, detW, detH, detWpx, detHpx) 

    return det