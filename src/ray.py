import math
import matplotlib.pyplot as plt
import numpy as np
import src.mirror as mirror
from joblib import Parallel, delayed
from enum import Enum 



class SourceType(Enum):
	SOURCE_UNIFORM = 1
	SOURCE_CIRCULAR_GAUSS = 2
	SOURCE_CUSTOM = 99

class Ray:    
	def __init__(self, enter_point, enter_angle): # angle in degrees
		self.angle = enter_angle
		self.enter_point = enter_point
		self.u = (math.cos(self.angle), math.sin(self.angle))      # směrový vektor
		self.numberReflections = 0
		
class ParallelRays:
	def __init__(self, enter_y_min, enter_y_max, enter_angle, num_rays):
		self.rays = []
		h = (enter_y_max - enter_y_min)/(num_rays - 1) 
		for i in range(0, num_rays):
			self.rays.append(Ray((0, enter_y_min + i*h), enter_angle))
			
		   
class Ray3D:
	def __init__(self, enter_point, u, intensity, numStacks):
		self.enter_point = enter_point
		self.u = u
		uNorm = np.linalg.norm(u)
		if uNorm > 0:
			self.u = u/uNorm
				
		self.intensity = intensity
		self.lastMirror = np.empty(numStacks, mirror.Mirror3D)
		self.nextMirror = np.empty(numStacks, mirror.Mirror3D)
		self.numberReflections = 0
        
	def __str__(self):
		s = "Ray3D(%s,%s,%s)" % (np.array2string(self.enter_point), 
			np.array2string(self.u), np.array2string(self.intensity))
		return s
		
	def __repr__(self):
		return self.__str__()
		
		
class RaySourceParallel:
	def __init__(self, center, direction, numMirrorStacks):
		self.center = center		
		direction = direction/np.linalg.norm(direction)
		self.direction = direction
		self.numStacks = numMirrorStacks
	
	def processRays(self, width, height, start, uW, uH, row, intensity, direction, numStacks, setup, j, noCPUs):
		ray = None
		image = np.zeros((setup.detector.pix_x_no, setup.detector.pix_y_no))
		for k in range(j, width, noCPUs):
			for i in range(height):
				if (row[k][i] != 0) :
					origin = start + i*uW + k*uH
					ray = Ray3D(origin, direction, intensity, numStacks)
					img = setup.plot_ray(ray)
					image[img[1]][img[2]] += img[0] * row[k][i]
		return image

	def processRectGrid(self, orientation, size, type, matrix, intensity, setup):
    		
		if type == SourceType.SOURCE_UNIFORM:
			width = matrix[0] # number of rays in the x-direction
			height = matrix[1] # number of rays in the y-direction
			processMatrix = np.ones((width, height))

		if type == SourceType.SOURCE_CUSTOM:
			width = len(matrix) 			# number of rays in the x-direction
			height = len(matrix[0]) 		# number of rays in the y-direction
			processMatrix = matrix
			
		orientation = orientation/np.linalg.norm(orientation)
		uW = orientation*(size[0]/width) # width-expanding vector
		uH = np.cross(orientation, self.direction)*(size[1]/height) # height-expanding vector
		start = self.center - uW*(width)/2 - uH*(height)/2
		
		# print("Number of rays: ", width*height, flush=True)
		image = np.zeros((setup.detector.pix_x_no, setup.detector.pix_y_no))
		noCPUs = setup.noCPUs
		print("Using %d CPUs/Threads" % (noCPUs))
		if((height * width) < 1000 * noCPUs):
			image += self.processRays(width, height, start, uW, uH, processMatrix, intensity, self.direction, self.numStacks, setup, 0, 1)
			# print("No Parallel")
		else:
			img = Parallel(n_jobs=noCPUs)(delayed(self.processRays)(width, height, start, uW, uH, processMatrix, intensity, self.direction, self.numStacks, setup, j, noCPUs) for j in range(noCPUs))
			for i in range(len(img)):
				image += img[i]

		if(setup.detector.firstImage == True):
			setup.detector.firstImage = False
			setup.detector.pix_ar = image
		else:
			setup.detector.pix_ar += image