import math
import numpy as np
import copy

class Mirror:	 
	def __init__(self, p1, p2): # defined by two points
		self.p1 = p1
		self.p2 = p2
		self.length = math.sqrt((p2[0] - p1[0])**2 + (p2[1] - p1[1])**2)
		self.angle = math.atan2(p2[1] - p1[1], p2[0] - p1[0])
		self.u = (p2[0] - p1[0], p2[1] - p1[1]) # směrový vektor
		
class Mirror3D:
	def __init__(self, A, B, C, D, vThick, orient): # defined by three points to form pallarelogram
		self.A = A
		self.B = B
		self.C = C
		self.D = D
		self.v = A-B
		self.w = C-B
		self.n = np.cross(self.v, self.w)
		# self.vThick = vThick
		self.orient = orient
		
	def __str__(self):
		return "[Mirror3D]\nA=" +np.array2string(self.A) + "\nB="+ np.array2string(self.B) + "\nC=" + np.array2string(self.C) + "\nD=" + np.array2string(self.D) + "\n"

 
class MirrorStack:
	# init basic stack parameters, [-, mm]
	def __init__(self, mCount, mDepth, mWidth, mThick, mSpacing, doubleSided):
		self.mCount = mCount # number of mirrors in the stack
		
		self.mDepth = mDepth # depth of mirror, along radial
		self.mWidth = mWidth # width of mirror, perpendicular to depth
		self.mThick = mThick # thickness of mirror
			
		self.mSpacing = mSpacing # spacing of mirrors (in the center)
	
		self.doubleSided = doubleSided # are the mirrors double-sided?

		self.mirrors = np.empty(mCount, Mirror3D) # array of mirrors for this stack (Mirror3D)
			
	# sets the orientation of the init'd stack
	def setOrientation(self, vFwd, vFocus):
		self.vFwd = vFwd # forward vector, from center of curvature to focus point
		self.vFwd = self.vFwd / np.linalg.norm(self.vFwd) # normalise
		
		self.vFocus = vFocus # focus vector, perpendicular to vFwd
		self.vFocus = self.vFocus / np.linalg.norm(self.vFocus) # normalise
		
		self.vSide = np.cross(self.vFocus, self.vFwd) 
		self.transMat = np.vstack((self.vFwd, self.vFocus, self.vSide)).transpose()
		self.invTransMat = np.linalg.inv(self.transMat)
		#print("Stack transMat:\n", self.transMat)

	# sets given focus point and focal length, calculates center and radius
	def setFocus(self, focusPoint, focusLenth):
		self.focusPoint = focusPoint 	# desired focus point in 3D
		self.focusLength = focusLenth# - (self.mDepth*self.mThick)/(4*self.mSpacing)
		self.radius = self.focusLength*2   
		
		self.centerPoint = self.focusPoint - self.focusLength * self.vFwd
		self.dTheta = np.arcsin(self.mSpacing/self.radius) # angle delta between mirrors

		radiusSquared = self.radius*self.radius
		self.dTheta = np.arccos((2*radiusSquared - self.mSpacing*self.mSpacing)/(2*radiusSquared))

		self.thickDth = np.arcsin(self.mThick/(2*(self.radius+self.mDepth/2)))  # angle delta due to thickness, halved

	# generate the mirror array (after the params have been set up)
	def genMirrors(self):
		dHalf = self.mDepth / 2
		wHalf = self.mWidth / 2
		tHalf = self.mThick / 2
		N = self.mCount
		# N = int(self.mCount/2)
		Nhalf = np.floor(N/2)
		dTh = self.dTheta 
		thMin = -dTh * (Nhalf - 0.5) # minimum angle
		thMax = dTh * (N - Nhalf - 0.5) # maximum angle
		
		self.thickDth = np.arcsin(self.mThick/(2*(self.radius+self.mDepth/2)))  # angle delta due to thickness, halved
		self.dTh = dTh
		self.thMin = thMin
		self.thMax = thMax
				
		th = thMin

		# self.mCount = self.mCount * 2
		# self.mirrors = np.empty(self.mCount, Mirror3D) # array of mirrors for this stack (Mirror3D)

		for i in range(N):
			v = np.sin(th)*self.vFocus + np.cos(th)*self.vFwd # vector from center to center of mirror
			n = np.cross(v, self.vSide)
			tOffset = np.sign(th)*n*tHalf; # thickness offset of mirror;

			A1 = self.centerPoint + v * (self.radius + dHalf) + self.vSide * (-wHalf) - tOffset
			B1 = self.centerPoint + v * (self.radius - dHalf) + self.vSide * (-wHalf) - tOffset
			C1 = self.centerPoint + v * (self.radius - dHalf) + self.vSide * (+wHalf) - tOffset
			D1 = self.centerPoint + v * (self.radius + dHalf) + self.vSide * (+wHalf) - tOffset
			# A1 = self.centerPoint + v * (self.radius + dHalf) + self.vSide * (-wHalf) 
			# B1 = self.centerPoint + v * (self.radius - dHalf) + self.vSide * (-wHalf) 
			# C1 = self.centerPoint + v * (self.radius - dHalf) + self.vSide * (+wHalf)
			# D1 = self.centerPoint + v * (self.radius + dHalf) + self.vSide * (+wHalf) 			
			

			vThick = n*tHalf # thickness offset of mirror
			orient = 0 if self.doubleSided else np.sign(th)
			# primary side
			#mirror = Mirror3D(A+tOffset,B+tOffset,C+tOffset)
			mirror = Mirror3D(A1, B1, C1, D1, vThick, orient)
			self.mirrors[i] = mirror


			# 



			# A1 = self.centerPoint + v * (self.radius + dHalf) + self.vSide * (-wHalf) 
			# B1 = self.centerPoint + v * (self.radius - dHalf) + self.vSide * (-wHalf) 
			# C1 = self.centerPoint + v * (self.radius - dHalf) + self.vSide * (+wHalf)
			# D1 = self.centerPoint + v * (self.radius + dHalf) + self.vSide * (+wHalf) 

			# A2 = self.centerPoint + v * (self.radius + dHalf) + self.vSide * (-wHalf) + tOffset
			# B2 = self.centerPoint + v * (self.radius - dHalf) + self.vSide * (-wHalf) + tOffset
			# C2 = self.centerPoint + v * (self.radius - dHalf) + self.vSide * (+wHalf) + tOffset
			# D2 = self.centerPoint + v * (self.radius + dHalf) + self.vSide * (+wHalf) + tOffset
			

			# mirror = Mirror3D(A1, B1, C1, D1, True)
			# self.mirrors[i] = copy.deepcopy(mirror)

			# mirror = Mirror3D(A1, B1, C1, D1, False)
			# self.mirrors[i*2] = copy.deepcopy(mirror)
			
			# mirror = Mirror3D(A2, B2, C2, D2, True)
			# self.mirrors[i*2+1] = copy.deepcopy(mirror)
			
			th += dTh
		
	
	def __str__(self):
		s = "[MirrorStack]\n"
		s += "centerPoint = " + np.array2string(self.centerPoint) + "\n"
		s += "radius = " + str(self.radius) + "\n"
		s += "focusPoint = " + np.array2string(self.focusPoint) + "\n"
		s += "focusLength = " + str(self.focusLength) + "\n"
		s += "vFwd = " + np.array2string(self.vFwd) + "\n"
		s += "vFocus = " + np.array2string(self.vFocus) + "\n"
		s += "dTh= %.2f deg, thMin= %.2f deg, thMax=%.2f deg" % (self.dTh/np.pi*180, self.thMin/np.pi*180, self.thMax/np.pi*180)
		return s
		
	def __repr__(self):
		return self.__str__()
			
	def printMirrors(self):
		for m in self.mirrors:
			print(m)
			
	# compute a ray intersection, returning the ray's direction vector's
	# scaling parameter t (=inf if no intersection happens)
	def findIntersect(self, ray):
		#print("=========================")
		#print(ray)
		
		tOut = np.inf
		mirrOut = -1
		nextMirr = -1
		nextMirr = -1
		orMatch = False
		
		# find ray intersection with (front) aperture surface (cylinder x line in 3D)
		
		v = np.dot(self.invTransMat, ray.u)
		#print("v=", v)
		
		v2 = v[0:2]		
		#print("v2=", v2)
		
		n2 = np.array([-v2[1], v2[0]])
		#print("n2=", n2)
		
		b = ray.enter_point - self.centerPoint 
		b = np.dot(self.invTransMat, b)
		b2 = b[0:2]
		#print("b2=", b2)
		
		A2 = np.vstack((v2, n2)).transpose()
		
		#print("A2=", A2, ", b2=", b2)
		x = np.linalg.solve(A2, -b2)

		t = x[0]
		s = x[1]
		
		R = self.radius + self.mDepth/2
		
		# if solution exists, then
		if (s < R):		
			t0 = np.sqrt(R*R - s*s)		
			#print("t=", t, ", s=", s, ", t0=", t0)
				
			isectPoint = b2 + (t-t0) * v2
			isect3 = np.array([isectPoint[0], isectPoint[1], 0])
			isect3 = np.dot(self.transMat, isect3)
			
			#print("isect=", isectPoint, "\nisect3=", isect3)
			
			# angle between vFwd and intersection point on aperture surface
			theta = np.arctan2(isectPoint[1], isectPoint[0])			
			#print("theta= %.2f deg" % (theta/np.pi*180))

			thOff1 = np.inf
			thOff2 = np.inf
			
			# resolve the angle to the index of the mirror the ray might hit
			
			iMirr = -1
			
			if (theta < self.thMin):
				#print("below aperture")
				if s > 0:
					iMirr = 0
				thOff1 = theta - self.thMin
			elif (theta > self.thMax):
				#print("above aperture")
				if s < 0:
					iMirr = self.mCount - 1
				thOff1 = theta - self.thMax
			else:
				#print("inside aperture")
				theta = theta - self.thMin
				iMirr = int(theta / self.dTh)
				if s > 0:
					nextMirr = iMirr						
					iMirr += 1
				else:
					nextMirr = iMirr + 1

				thOff1 = (iMirr)*self.dTh - theta				
				thOff2 = (nextMirr)*self.dTh - theta
			#print("thOff = %.3f deg" % (thOff/np.pi*180))
			if (abs(thOff1) < self.thickDth) or (abs(thOff2) < self.thickDth):
				iMirr = -1
				tOut = 0
			
			if iMirr != -1 and iMirr < self.mCount:
				#print("mirr #=", iMirr)				
				mirrOut = iMirr
				#adjust for mirror thickness
				(tOut, orMatch) = ray_mirror_intersection(ray, self.mirrors[mirrOut], np.sign(s))
				if not orMatch:
					nextMirr = -1
			else:
				mirrOut = 0
				nextMirr = -1
								
		return (tOut, orMatch, mirrOut, nextMirr)
		
def ray_mirror_intersection(ray_in, mirr, dir):
	#A = np.array([[ray_in.u[0], -mirr.v[0], -mirr.w[0]],[ray_in.u[1], -mirr.v[1], -mirr.w[1]], [ray_in.u[2], -mirr.v[2], -mirr.w[2]]])
	A = np.vstack((ray_in.u, -mirr.v, -mirr.w)).transpose()
	t = np.inf
	orMatch = (mirr.orient == 0) or (mirr.orient == dir)

	if (np.linalg.matrix_rank(A) == 3): # ray is not collinear with the mirror plane					
		b = mirr.B - ray_in.enter_point # offset by mirror thickness in "dir" direction
		# b = mirr.B - mirr.vThick * dir - ray_in.enter_point # offset by mirror thickness in "dir" direction
		x = np.linalg.solve(A,b)

		t = x[0]
		s = x[1]
		r = x[2] 

		if s < 0 or s > 1 or r < 0 or r > 1 or t <= 0 : # ray outside of mirror bounds 
			t = np.inf

	return (t, orMatch)
