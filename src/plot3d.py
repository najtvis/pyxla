from mpl_toolkits.mplot3d import Axes3D
from mpl_toolkits.mplot3d.art3d import Poly3DCollection
import matplotlib.pyplot as plt
import numpy as np


class Plotter3D:
	def __init__(self, x_lim, y_lim, z_lim, azim=-135, elev=20):

		
		self.fig = plt.figure(figsize=(12,5))
		self.ax = self.fig.add_subplot(111, projection='3d')
		
		self.x_lim = x_lim
		self.y_lim = y_lim
		self.z_lim = z_lim

		self.x_size = x_lim[1] - x_lim[0]
		self.y_size = y_lim[1] - y_lim[0]
		self.z_size = z_lim[1] - z_lim[0]

		self.ax.set_xlim((x_lim[0], x_lim[1]))
		self.ax.set_ylim((y_lim[0], y_lim[1]))
		self.ax.set_zlim((z_lim[0], z_lim[1]))
		
		self.ax.view_init(azim=azim, elev=elev)
		self.ax.set_proj_type('persp') 
		self.ax.margins(x=2, y=2, z=1)

		plt.axis('off')
		plt.grid(False)

	def plot_line_seg(self, A, B, clr, ord='average', alph=0.10):
		X = [A[0], B[0]]
		Y = [A[1], B[1]]
		Z = [A[2], B[2]]
		vertices = [list(zip(X,Y,Z))]

		poly = Poly3DCollection(vertices, color = clr, linewidths=1, alpha=alph)
		poly.set_zsort(ord)
		poly.set_alpha(alph)
		surf = self.ax.add_collection3d(poly)
  
	def is_point_in_fig(self,x,y,z):
		return (x>=0 and x<=self.x_size) and (y >= 0 and y<=self.y_size) and (z >= 0 and z <= self.z_size)  

	def plot_rectangle(self, A, B, C, D, clr):
		X = [A[0], B[0], C[0], D[0]]
		Y = [A[1], B[1], C[1], D[1]]
		Z = [A[2], B[2], C[2], D[2]]
		vertices = [list(zip(X,Y,Z))]

		poly = Poly3DCollection(vertices, color = clr, edgecolor='black', linewidths=.1)
		surf = self.ax.add_collection3d(poly)

if __name__ == '__main__':

	pltr3D = Plotter3D(20,20,20)
	pltr3D.plot_line_seg((10,10, 10), (10, 10, 20), 'r')
	pltr3D.plot_line_seg((15,15, 0), (10, 10, 10), 'r')
	pltr3D.plot_rectangle((0,0,0), (5,0,0), (7.5,7.5,0),  'r' )
	plt.show()
