#%% ====================================================
# Import libraries
#=======================================================
import src.setup3D as setup3D
import src.ray as ray
import src.objects as objects
import src.functions as functions

from pickle import TRUE
from tokenize import String
import time
import math
import numpy as np
import matplotlib.pyplot as plt


#%% ====================================================
# 1, Initial setup
#=======================================================
tStart = time.time()

# create output folder if not exists
outputFolder = "timepix"
outputFilePrefix= "2keV_adj_"
functions.createDir(outputFolder)

# Decision if the 3D will be printed out and includes rays
# Do not print out more than 1000 rays, otherwise it consumes too much RAM!
# True = yes, False = no
draw3D   = False
drawRays = False

#%% ====================================================
# 1.1, set detector
#=======================================================
# set detector type timepix, timepix512, timepix_div_10, tpx_quad
# in case of different type of detector, add it in file src/objects.py
detectorType = 'timepix'  

#%% ====================================================
# 1.2, set source
#=======================================================

# resolution in H and V axis
# 1 means full, 2 means half, ...
# requires to be 
divH = 1
divV = 1

# all spatial dimensions are in milimeters
divXmm = 0.0275
divYmm = 0.0275

angleX = 0.000000000    # in degrees
angleY = 0.000000000    # in degrees

# source: set number of rays and its spacing
noPoints = (1000,1000)  

# Define input points
focusPoint = np.array([0.00001, 0.00001, 0.00001])     # detector position 
detectorOffset = np.array([-17.5,0,0])
vStackOffset = np.array([0,0,0])
hStackOffset = np.array([0,0,0])

fwhmMeas = []
peakMeas = []
offset = [0]

#%% ====================================================
# 1.3, define search ranges
#=======================================================

for i in range(-100, -45, 1):
    offset.append(i)

for i in range(16, 25, 1):
    offset.append(i)

for i in range(45, 101, 1):
    offset.append(i)

print(offset)

# perform search for all offsets of optics
for i in offset:
    detectorOffset = np.array([i,0,0])

    #%% ====================================================
    # 2, create mirror module stack
    #=======================================================
    mirrors = []

    vStack = objects.createMirrorModule(47, 150, 75, 0.35, 0.75, True, 'V', focusPoint + vStackOffset, 965*2) 
    aperturePoint = np.array([-vStack.focusLength - vStack.mDepth/2, 0, 0])
    mirrors.append(vStack)

    #%% ====================================================
    # 3, create detector
    #=======================================================

    det = objects.createDetector(focusPoint + detectorOffset, detectorType)

    #%% ====================================================
    # 4, create setup 
    #=======================================================

    # creates a setup object with given mirrors and detector, numerical
    # parameters define the size of the 3D plot space, and the last param
    # controls whether this plot is shown or not
    stp = setup3D.Setup3D(np.array(mirrors), det, drawRays)

    #%% ====================================================
    # 5, set energies
    #=======================================================

    # define the energies of the sources used in the setup
    # - first param sets energy in eV
    # - second param contains reflectivity data: 
    #	- if numeric, then the same value is used for all angles of incidence 
    # 	- if string, then treated as a filename for text data with format 
    #	  according to [http://henke.lbl.gov/optical_constants/layer2.html]
    #stp.appendEnergy(0, 1) # energy no. 1, independent on angle (visible light) 
    # stp.appendEnergy(1487, "./inputs/reflectivity/1487eV.dat") #  0...5 degs
    stp.appendEnergy(2123, "./inputs/reflectivity/2123eV.dat") #  0...5 degs
    # stp.appendEnergy(8048, "./inputs/reflectivity/8048eV.dat") #  0...2 degs
    # stp.appendEnergy(19279, "./inputs/reflectivity/19279eV.dat") #  0...2 degs

    # set the number of channels for the detector to the number of energies used
    numEnergies = len(stp.energies)
    det.setChannels(numEnergies)  

    #%%=====================================================
    # 6, create radiation sources
    #=======================================================
    
    inputAperture = (noPoints[0]*divXmm, noPoints[1]*divYmm)
    srcZposition = -1500
    srcPosition = np.array([srcZposition, ((srcZposition + aperturePoint[0]) * math.tan(math.radians(angleX))), ((srcZposition + aperturePoint[0]) * math.tan(math.radians(angleY)))]) 

    direct = aperturePoint - srcPosition
    direct = np.array(direct / np.linalg.norm(direct))

    # initialise all intensities to 1 (all rays emit all energies with unit intensity)
    inten = np.ones(numEnergies)

    # create the source given the center, direction and number of mirror stacks
    src1 = ray.RaySourceParallel(srcPosition, direct, stp.mirrorStacks.size)

    #%% ====================================================
    # 7, run simulation
    #=======================================================

    tStart = time.time()
    src1.processRectGrid([0, 1, 0], inputAperture, ray.SourceType.SOURCE_UNIFORM, (int(noPoints[0]/divH), int(noPoints[1]/divV)), inten, stp) 
    tEnd = time.time()
    print("Execution time for setup: %.2f s for %d rays" % (tEnd-tStart, noPoints[0]*noPoints[1]))

    #%% ====================================================
    # 8, plot and save results
    #=======================================================
    x=np.linspace(1,stp.detector.pix_x_no, num=stp.detector.pix_x_no)

    fig = plt.figure()
    plt.imshow(stp.detector.pix_ar)	
    plt.title("Detector image for E = %d eV and offset %d" % (stp.energies[0], i ))
    plt.xlabel("x [px]")
    plt.ylabel("y [px]")
    fig.savefig(outputFolder + '/' + outputFilePrefix + str(i)+'.jpg', format='jpg', bbox_inches="tight")
    np.savetxt(outputFolder + '/' + outputFilePrefix + str(i)+'.txt',stp.detector.pix_ar,fmt='%.5f')
